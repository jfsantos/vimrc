set nocompatible
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'altercation/vim-colors-solarized'
Plugin 'vimwiki/vimwiki'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'ap/vim-buftabline'
Plugin 'JuliaEditorSupport/julia-vim'
Plugin 'octref/RootIgnore'
Plugin 'rust-lang/rust.vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'vim-airline/vim-airline'
Plugin 'majutsushi/tagbar'

call vundle#end()

if has('gui_running')
  set guifont=Ubuntu\ Mono:h16
endif

set termguicolors
set background=dark
colorscheme solarized
syntax enable
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set backspace=2
set number
set showcmd
filetype indent on
filetype plugin on
set wildmenu
set showmatch
set incsearch
set hlsearch
set ruler
set hidden

if has("gui_running")
    set lines=60 columns=95
endif

let mapleader=","

" Shortcuts
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
nnoremap <Leader><Left> :bprev<CR>
nnoremap <Leader><Right> :bnext<CR>
nnoremap Q :bd<CR>
